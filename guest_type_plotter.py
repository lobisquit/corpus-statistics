import matplotlib.pyplot as plt
import pandas as pd
from matplotlib import cm, rcParams


def create_pie_plot(sizes, labels, out_file, title,
                    explode=None, colors=None, angle=140,
                    **kwargs):

    if not colors:
        colors = cm.Set2([i / len(sizes) for i in range(len(sizes))])

    if isinstance(explode, float):
        explode = [explode for _ in sizes]

    fig = plt.figure(frameon=False)
    ax = fig.gca()
    ax.pie(sizes,
           explode=explode,
           labels=labels,
           colors=colors[:len(sizes)],
           autopct='%1.1f%%',
           startangle=angle,
           **kwargs)

    ax.set_title(title)
    ax.axis('equal')
    plt.savefig(f'plots/{out_file}')


rcParams['font.family'] = 'serif'

data = pd.read_csv('data/guest_type.csv')

common_count = data['guest_type'].apply(lambda el: 'comune' in el).sum()
celebrity_count = data['guest_type'].apply(lambda el: 'Celebrità' in el).sum()

male_count = data['guest_type'].apply(lambda el: '(M)' in el).sum()
female_count = data['guest_type'].apply(lambda el: '(F)' in el).sum()

create_pie_plot(sizes=(common_count, celebrity_count),
                labels=('Persona comune', 'Celebrità'),
                # colors=('#eff44e', '#85f44e'),
                explode=0.04,
                out_file='celebrity.png',
                title='Tipologia di ospiti nel corpus analizzato')

create_pie_plot(sizes=(male_count, female_count),
                labels=('Maschile', 'Femminile'),
                colors=('#4ed9f4', '#f44eef'),
                explode=0.03,
                out_file='gender.png',
                title='Sesso degli ospiti nel corpus analizzato')
