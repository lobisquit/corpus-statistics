import re
from pathlib import Path

MAX_LINE_LENGTH = 60
INPUT_PATH = Path('transcripts/inputs/')
OUTPUT_PATH = Path('transcripts/converted/')

OUTPUT_PATH.mkdir(exist_ok=True)


def read_content(f):
    for raw_line in f.readlines():
        line = raw_line.replace('--', ' -- ') \
                       .replace('  ', ' ') \
                       .strip()

        if line:
            yield line


def collapse_lines(content):
    speaker_pattern = re.compile(r'^(?P<speaker>[A-Z])\t(?P<sentence>.*)$')

    current = {'speaker': '', 'sentence': ''}

    for line in content:
        matches = speaker_pattern.findall(line)

        if matches:
            # yield the previous sentence, if not empty
            if current['sentence']:
                yield current['speaker'], current['sentence'].lstrip()

            # replace current with just found match
            speaker, sentence = matches[0]
            current = {'speaker': speaker, 'sentence': sentence}
        else:
            # append to the previous one otherwise
            current['sentence'] += ' ' + line

    # last sentence
    if current['sentence']:
        yield current['speaker'], current['sentence'].lstrip()


def truncate_line(sentence, line_length=MAX_LINE_LENGTH):
    current_chunk = ''
    for word in sentence.split(' '):
        if len(f'{current_chunk} {word}') > line_length:
            # return previous chunk if limit would be exceeded
            yield current_chunk.lstrip().ljust(line_length)

            # replace it with just the current word
            current_chunk = word
        else:
            current_chunk += ' ' + word

    # last chunck
    yield current_chunk.lstrip().ljust(line_length)


total_f = open(OUTPUT_PATH / 'total.txt', 'w')

for path in sorted(INPUT_PATH.glob('*.txt')):
    total_f.write(path.name + '\n')

    # ensure output path exists
    OUTPUT_PATH.mkdir(exist_ok=True)

    speakers = set()

    with open(path, 'r') as input_f:
        with open(OUTPUT_PATH / path.name, 'w') as output_f:
            # read input file
            content = read_content(input_f)

            # obtain clear (speaker: sentence) mapping
            dialog = collapse_lines(content)

            n_line = 1
            for speaker, sentence in dialog:
                speakers.add(speaker)

                # split line if longer than limit
                for chunk in truncate_line(sentence):
                    line = speaker.ljust(4) + chunk + str(n_line).rjust(5)
                    output_f.write(line + '\n')
                    total_f.write(line + '\n')
                    n_line += 1

                    # reset speaker marker after first line
                    speaker = ''

    # total_f.write('\n\n')

    print(path.name, 'speakers =', speakers, 'n_lines =', n_line)
total_f.close()
