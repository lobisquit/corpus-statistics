O	[MUSIC - LIONEL RICHIE, "ALL NIGHT LONG"] All night long.
	Oh. All night long. Whoa. All night long. All night long.
	Whoa! Once you get started, you can't sit down. Come join
	the party, a merry --
J	Oh good, you got my flowers.
E	Thank you for sending them.
J	You're welcome. Every day I do it. It's very expensive.
E	I know. Every day. They're from you?
J	You do not need them. But I send them to you every day.
E	So sweet to do so.
E	I was looking through old pictures. You know how when you
	get bored and you're just looking through old stuff on your
	phone and all your photos? And I came across me with that
	song playing in the car when I was driving that I sent to
	you. Cause that song reminds me of --
J	Do you have the picture?
E	I don't have it with me. No. It was a whole -- I love the
	picture.
E	It was a whole video.
J	But yeah.
E	It was a whole thing. Yeah.
J	We've been doing this for years.
E	It's his favorite song. He's a wonderful dancer. He
	stretches as you can see.
J	As you can tell, I am a wonderful --
E	You are
J	-- Dancer.
E	But what's important is that you stretch. Because you know
	you need to limber up.
J	You have to. Hammies would go like that.
E	Cause you dance all night long. Hey. I loved your Super
	Bowl commercial.
J	Thank you very much.
E	I loved it.
J	I loved yours.
E	Thank you. It was very funny. And --
J	Thank you.
E	Y'all all got to use your accents.
J	We got to use our fake accents. Yeah. Everybody's like,
	where from Boston are you? And I'm like, the streets.
	They're like, whereabouts? I'm like, Newton, which is like
	the most suburban part of Boston and if you're from Boston,
	you find that funny. Obviously, none of you are.
E	No. No.
E	Oh!
J	We got one over there. Yeah. They just didn't think it was
	funny.
J	She's like, the Newton joke killed! Yeah. Thank you. Thank
	you.
E	So you knew Chris Evans already?
J	I did. We weirdly went to rival high schools at the same
	time. And I think we might have played sports against each
	other, but didn't know.
E	You did audition for the same role.
J	That's very nice to say I auditioned for the same role. I
	did. The truth is, they hadn't offered it to them yet. So
	they were like, let's see who else is out there before we
	offer it to Chris Evans. And I went in and I tested for
	"Captain America." Got to wear the suit. Which was really
	fun. This is a true story. I was putting the suit on. And
	the guy was like, this is really momentous. And I said, yes.
	And I was putting the suit on, and I was halfway up, not
	wearing any other clothes other than this. And I was halfway
	up and I was like, I think -- and right at that moment,
	Chris Hemsworth walked by and he was like, you look good,
	mate. And I was like, nope. You know what? It's fine. We
	don't have to do this. Nope. We don't. He was just like,
	jacked. He's like, you're going to look great in that suit.
	And I was like, don't make fun of me Hemsworth. And so I
	just walked away right there.
E	No, you did not.
J	No, I didn't. No. I acted my heart out that day. And it
	didn't work out.
E	It didn't work out. But that's all right. You went a
	different direction. And by the way, you look great. Because
	you have been working out like crazy. Yes. For everything
	that you've been doing.
J	Like this. For this show.
E	For this show. Yeah. I like to gain weight for a show.
	What I do is I try to gain weight in case there's a show
	coming up.
J	That's -- Speaking of shows, congratulations on your
	Golden Globe. That was huge.
E	Oh thank you. That was -- That was huge, wasn't it?
	[CHEERING] : And that was -- I'm a comedy nerd. That is the
	art of comedy is the most brilliant comedian can take a joke
	and keep going with it and going with it and make it funnier
	and funnier. I make them worse. But you made it very, very
	funny.
E	not true.
J	That was the greatest speech.
E	Thank you. I had a lot of fun. Thanks. So, well let me
	compliment you now. We're just going to compliment each
	other back and forth. You, again, made the cut of People's
	Sexiest Man. Not -- [CHEERING] Not alive.
J	No, I didn't even know that actually. Because, as your
	producer was describing where I was in the magazine, she was
	like, sort of in the back, like, back, and then like back.
	And I was like, like in the appendix? Like where -- was I
	the honorable mention? Or like, your mom phoned in and said
	we should remember you? I was like, what section is this?
	Yeah.
E	If you were to vote, who would be the sexiest man, in your
	opinion?
E	I mean - I - To be really honest, I thought Brad Pitt was
	the sexiest person every year. I mean, like - He must get
	the phone call. And he just goes, yeah, no. Guys, somebody
	else is here. And they're like, great. We just had to ask.
	And now we'll go to somebody else. But he has to be. And
	then speaking of Golden Globes, he made that speech. And on
	top of looking like that, he's like, do something nice. I
	was like, all right. We get it. You win at life. Move on.
E	Yeah He's a good guy.
J	He's a wonderful person.
E	You know that. He's a really good guy. Uhm I just saw that
	Hazel, your daughter -- that's her name.
J	That is.
E	Reminding you.
J	Thank you.
E	Is six years old.
J	She's about to be. Coming up.
E	Is that the one that I met at the San Ysidro Ranch?
J	Yes. She was tiny.
J	Tiny.
E	She was a tiny little girl.
J	Like this. Yeah.
E	She was like that.
J	She was like a Treasure Troll.
E	She was -- you would carry her in your hand like that.
J	And just do this with her hair. Yeah.
E	And what is she --
J	And now she's six.
E	What is she doing now? What is she into?
J	Um, making me laugh. She's so funny. She -- the other day,
	I was actually, uhm after 10 years of being with my wife, I
	was like, I'm finally going to do a photo wall. So I got all
	these brand new frames and I unwrapped them and I put them
	on the ground. You put them on the ground before you put
	them up where they're going to be on the wall. And the girls
	came over, and they're like, wow. You're doing such a great
	job, Daddy. And then Hazel literally leaned on my shoulder
	and she went, so good. Don't you think you should put
	pictures of us up? It was all the fake families in the
	frames.
E	That's funny.
J	It was amazing.
E	She's fun -- and good timing.
J	Yes.
E	To pause like that. : I think she was genuinely concerned
	that I was losing my mind.
E	That's really cute. All right. We have to take a break.
	There's a commercial.
J	Oh man.
E	But this is what happens. We talk, then there's a
	commercial. And then I say, we'll be back. And then when
	they come back, we're still sitting here.
J	Let's do it. You'll see what happens.
J	OK.
E	We'll be back.
