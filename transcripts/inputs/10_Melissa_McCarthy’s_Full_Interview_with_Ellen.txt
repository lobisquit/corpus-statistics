M	Where's my wig?
E	I --
M	For God sakes --
E	I just heard that you go there.
M	I'm at Wilshire Wig, like, at least once a month. I was
	just in there a week and a half ago.
E	Really?
M	Yes.
E	What do you do there?
M	What don't I do there?
E	Yeah.
M	This is all built around a wig. No, every time I start a
	movie, I start with a wig. And I want to know what she looks
	like, so I go to Wilshire Wig. Lenia is there. That's who
	helped me last time.
E	Is that her name?
M	I think -- I think --
E	It's Lenya.
M	Lenya.
E	Yeah. Yes.
E	Like Kenya.
M	Like Kenya -- oh, oh. That's helpful. Lenya like Kenya.
	OK, that's helpful.
E	OK.
M	And I tried on, like, a million wigs. And then I find one.
	I'm like "oh, it's like this but this color." And then I do
	a hybrid, and then we make one.
E	Yeah. Well, that's a good idea, because it really does
	change your personality when you put a wig on.
M	It really does.
E	Yeah.
M	And that short curly one you had on, I'm a big fan. I
	don't -- I would take that and just perm it and let it ride.
E	I permed this bef -- I mean, when I was, like, 16, I
	permed it.
E	Did you do ever a perm? Did you do a perm?
M	My Aunt Linda permed my hair, and I was very short around
	-- in the fourth grade. Not like the giant I am now. And she
	gave me --
E	You really shot up.
M	I really shot up, guys. The camera messes with it, but I'm
	7' 2". She gave me a short -- I wanted, like -- you know, I
	don't know. Because I have poker-straight Marcia Brady hair,
	and I wanted something different, and I got it. Because I
	had a short, tight, curly perm. And I was a little round.
	And so then this went round. And there's just a lot of
	pictures of me in, like, Terry cloth shorts, like -- (SMILES
	AND RAISES THUMB) It's --
E	Do you have a pic -- you don't have a picture?
M	Not on me. Yeah. (LAUGHING) My God!
E	You don't carry it?
M	I wish I did. How weird would that be?
E	I'm told I have mine. They keep mine handy. This is my
	perm picture.
M	Of your perm?
E	Yeah.
M	Oh!
E	Yeah. It's kind of like that wig.
M	It's wonderful.
E	Yeah. Yeah. And look how happy I was with it too. The
	regret is on my face immediately.
M	Oh, and I love that 45 degree angle that you had going on
	there.
E	Yeah.
M	Woo!
E	Thanks a lot.
M	I love it.
E	Thanks. So can you -- 20 times, you've been on this --
M	20 seems cra -- oh, wait. I want to commemorate. Here.
E	Oh, a bag!
M	It's a --
E	What?
M	Don't be -- It's --
E	What?
M	It's the finest of China. That's for your 20th. That's the
	gift for 20.
E	That's so nice.
M	And it may not be China, but it's --
E	Is this you?
M	-- from China. [LAUGHING]
M	Yes.
E	So 20 times -- besides this, obviously, because you put it
	on a mug, what is your -- I have a favorite time that you've
	been on, but what is yours?
M	I would have to say -- I mean, one of the -- I think the
	last time I was on is, to me, what I like to consider my
	first heart attack when Billie Eilish came out of that
	thing. And I watched it back, because I -- and I just kind
	of was like -- I don't think I looked as crazy as I felt,
	because I was so panicked. Cause I was like: "I'm going
	out.I'm going out." And I was so sparkly and jittery that I
	really -- I was like: "I'm going to go down. I'm going to
	hit the cup, then I'm going to hit the table, and I'm going
	to break out all my teeth."
E	Oh, that's not what we were going for.
M	And that's all I was thinking about. And then later I
	watched it and I was just like -- (MIMICKS MOVEMENT) so I
	guess that's how I panic.
E	Yeah. That was -- it was one of my favorite things that
	we've ever done. Billie Eilish -- and she -- Billie Eilish
	is -- like, you're a huge fan of her. She loves you.
M	Huge fan.
E	And she was about to go on tour. And she came here just to
	scare Melissa.
M	And, like, crawled under the stage --
E	Crawled under the stage to get -- and was in that box for
	so long waiting for the cue. This is Billie Eilish scaring
	Melissa.
M	Her mom was my first improv teacher -- No way! Oh, wow! --
	Maggie Baird, who is amazing. So they were -- I think when
	this came out, they were on tour.
M	Do you know that if she probably -- [SCREAM] Oh, my God!
E	We got those two also. We got to Elizabeth --
M	Yeah, everybody was like: [SCREAMING]
E	Yeah, yeah. And I would like to think that my instincts
	of, like, if I'm really scared, I'd like to think that I'd
	be like:[MIMICKS EXPRESSION] like, do something kind of
	amazing instead of just like -- [GASP]
E	Yeah.
M	Like, I just shut it down. I shut it down, and I did
	nothing.
E	Yeah, that's what most people do. Most people -- there are
	few people that pull their -- and they look like they're
	going to punch somebody. And I always worry that that's
	gonna happen, that our poor, little guy that's in there --
M	that someone's gonna -
E	-- that he's gonna get punched one day. So we pay him
	extra. [LAUGHING] Oh.
E	I mean -- no but, I don't mean poor he's a little guy. I
	just mean --
M	(LAUGHING) Oh, God.
E	-- poor guy.
M	Just poor, poor, poor guy. Just a poor, little guy.
E	Well, he's not a huge guy. He's in a box. He's got to be
	little. We're not going to put a --  -- giant guy in there.
M	It's getting worse.
E	Corey is - is a normal sized man.
M	For a box.
E	All right, so -- wait, we're going to take a break, and
	then we'll talk about this. Thanks, Andy. We'll take a
	break. I'm still new to this. We'll be back.
E	You gave it to Ben --
M	Yes. This was how I say --
E	-- for a birthday present.
M	-- I love you and I'm glad you were born.
E	OK, do you always give him -- before we show this, do you
	always give him strange birthday gifts? Because it's hard
	when you're together that --
M	Yeah, it's been like -- you know, we've been together over
	20 years. So I mean, I feel like his gifts get nicer and
	more thoughtful, and mine tend to be getting weirder. Also
	cause he doesn't want to -- like, he doesn't want to -- if I
	give him cute clothes or I'm like: "Oh, you'll look so cute
	in this," he's like: "Oh, pants." Like and there's a visible
	-- and I can't get him guitars --
E	Right.
M	Cause he has to pick them out. So then my mind starts to
	wander and --
E	So - so this is where it wandered to. So this is what she
	got --
M	Yeah. This is the natural jump.
E	-- her husband. So they painted a portrait -- you gave
	them a picture of the two of you.
M	Yep.
E	And someone made both of you --
M	And I said, "What about sad, sad clowns?"
E	Right.
M	And they did such a good job my only note about halfway
	through, I was like: "let's -- let's go sadder." And I said,
	really, let's darken some of those lines and let's put a
	tear here or there. And Ben just made a very strange
	wheezing noise when he opened it. He was like: [LAUGHING
	WHEEZE] And the girls wanted noth -- they were like
	(DISGUSTED) "Oh, no!" And Georgie was like: "No! It's not
	hanging! It's not hanging!"
E	Is it?
M	It's about to be.
E	Yeah?
M	And I swear I'm going to -- now since I feel like there's
	been a challenge, because Georgie really dislikes it, so I
	want to have them painted. And I want to hang it next to it.
E	What a great mother you are.
M	Yeah. [LAUGHING] OK, so you've been on the show 20 times,
	and this is your 20th time, and you've never done this
	before. So today is the day. We are sending you to Milt &
	Edie's Drycleaners. I'm going to put the thing in your ear.
	You have to say --
M	It seems like a terrible idea, right?
E	Yeah. It's gonna be so good. So it is gonna be Ellen in
	your ear. You're goinna go to the dry cleaners --
M	Oh, God.
E	-- right now. You have to say and do everything I tell you
	to do.
M	OK. OK.
E	All right?
M	Deal.
E	All right, we're going to send you -- before we go to
	break, I want to give you a gift and then you'll head over
	to the dry cleaners. But I got something to hang in your
	home. Georgie is gonna love it. [LAUGHING]
M	Oh, my God! Thank you!
E	And I'm happy. I'm a happy clown!
M	I mean, that's a happy clown.
E	We'll be back!
