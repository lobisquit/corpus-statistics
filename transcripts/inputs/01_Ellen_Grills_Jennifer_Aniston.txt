J	I almost just perched myself right on the old arm there.
E	You should have done that and acted like nothing was
	wrong. Like, that's the way you were going to sit.
J	Well, that's what I did-ish.
E	Yeah, You handled it very well.
J	Hi!
E	It's good to see you again!
J	It's good to see you, always.
E	Always good to see you. I saw you a few weeks ago. I gave
	you the award, uh, Power Woman of Hollywood. You're the most
	powerful women -- woman -- women [APPLAUSE]
J	Maybe -- uh -- I love that you say that to me though. That
	means so much.
E	It's true. You got the award for it.
J	Oh, I di -- There was a couple women that got the award
	for it.
E	Right, but you were the --
J	But I was one of five, six.
E	But you were the most powerful of all of them. And let's
	be honest.
J	OK, Ellen.
E	yeah, OK. It was wonderful. It was fun.
J	It was fun.
E	The prompter broke.
J	It sure did.
E	Yeah.
J	You were there. Yeah, First of all, thank you for coming
	and doing that for me and introducing and giving it to me.
E	It was my pleasure and an obligation. [LAUGHTER]
J	uh, I understand.
E	As a best friend.
J	As a best friend -- well, OK.
E	So I said, she's one of my closest friends, I'm one of her
	closest friends. And then she comes out and says, "don't say
	that, I have a lot of best friends in the audience"
J	Well, I did.
E	Well, I know. But why don't you just play along with me
	and let me be your best friend?
J	I, yeah, Well, that's probably the -- the -- the "coda" in
	me -- co-dependent, taking care of my friend's feelings.
E	Oh.
J	You know.
E	Yeah. That's sweet of you. That's why you're my best
	friend!
J	Right! 'Cause -- Exactly.That's how I take care of you.
E	Good, And I take care of -- we -- I'm coda --
J	-- Take care of each other.
E	-- as well.
J	Great. and, We make a great match.
E	yeah, [LAUGHS]
J	It's a wonderful friendship.
E	That's why we're not together. -- uhm, so [LAUGHTER]
E	So yeah, so the -- the prompter broke.
J	Yes, the prompter broke.
E	They were trying to fix it. I went out and I --
J	You went out, and you danced with the electrical guy. That
	was --
E	I was giving him time to fix it. [PICTURE COMES UP:
	LAUGHTER] So I danced --
J	But wait. Did you show what you actually did to him?
E	Well, no.I kind of grinded on him didn't I?
J	You did.
E	I did.
J	Kind of? You did!
E	He didn't even know it.
J	Well, could you imagine if he was like: "oh let me take a
	moment and enjoy this" -- when the world is falling apart,
	in his mind?
E	Yeah, I know, poor guy. But anyway, so you -- you were
	wonderful.
J	thank you
E	And then you came the very next day to the Gorillapalooza
	event
J	Yes!
E	which was very sweet of you cause you had to get up early
	in the morning to do press.
J	I did, Yes.
E	So thanks for that.
J	Thank you. You're just saying all my good deeds for the
	weekend.
E	Yeah, because you did a lot of wonderful things.
J	I did a lot -- even, I went to visit you after the -- after
	the - uh -- after the award, on Friday, when you were
	getting a haircut.
E	You sure did. You stopped by. I get a text from Jennifer
	and it says, "do you have a belt?" And I'm like: "what do
	you mean do I have -- of course I have a belt. What do y(ou)
	-- Why are you asking if I have a belt?" And then you --
J	no, first she said -- you said, "this is Ellen, who are
	you texting?"
E	well, I just thought --
J	And I said, "I'm texting you!". I need a -- I need a belt.
E	I thought you'd be texting a stylist. I mean, why text me,
	"do you have a belt?"
J	Yeah. Yeah, sorry I --
E	Let's jump to this very important information --
J	uhm uhm
E	not an information yet.
J	Ok.
E	But you'll give it to us.
J	OK.
E	Uhm, When are you going to guest host the show?
J	Oh, This show?
E	Don't you want her to guest host this show? [CROWD
	CHEERING]
J	I'm -- I'm dying to guest host it.
J	So let -- when do we -- when do you want me to do it?
E	For my birthday, or your birthday.
J	OK. Or in between.
J	In between.
E	Yeah. Why don't you do that?
J	But you won't be here. Right? Yeah, The whole point is --
E	No, the whole point is you guest host, and I'm not here.
	[LAUGHTER]
J	Right.
E	Yeah. It's my day off, and then you're here for that.
J	Yeah,
E	yeah OK.
J	All right, great. Can I call you from the set?
E	mhh-mhh. [LAUGHTER]
J	Mary!
E	Yeah, you can. All right, Jen is here for a while. You're
	not leaving for a while, and we're going to play a really
	fun game, my favorite game.
J	What?
E	You'll see.
J	Oh.
E	And then she's going to make a big announcement about the
	"Friends" reunion, which is gonna be fantastic.
