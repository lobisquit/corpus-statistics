library(lubridate)
library(scales)
library(ggplot2)
library(data.table)
library(dplyr)
library(forcats)
library(extrafont)

normalit <- function(m) {
   m / sum(m)
}

## custom visual elements
loadfonts()
my_theme <- theme_bw() +
    theme(plot.title=element_text(hjust=0.5),
          text=element_text(family = 'DejaVu Serif'))

## read data
current <- read.csv(file='data/dm_functions.csv',
         header=TRUE,
         encoding='UTF-8') %>%
    mutate(combination=1:n()) %>%
    group_by(dm) %>%
    mutate(percentage=normalit(n)) %>%
    filter(n>0) %>%
    ungroup() %>%
    ggplot(aes(x=reorder(function., -n),
               y=percentage)) +
    geom_bar(stat='identity', show.legend=FALSE) +
    facet_wrap(~dm, scales='free') +
    ## scale_x_discrete(labels=function(x) {gsub(' / ', ' /\n', x)}) +
    scale_y_continuous(labels=scales::percent_format(accuracy=1)) +
    labs(x='Functions', y='Occurrences') +
    my_theme +
    theme(axis.text.x=element_text(angle=45, hjust=1),
          strip.text.x=element_text(face='italic'))

ggsave('plots/discourse_marker.png', current,
       width=16, height=20, units='cm')
